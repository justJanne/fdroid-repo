FROM ubuntu:latest

ENV TZ=Etc/UTC
ENV DEBIAN_FRONTEND=noninteractive

ADD src/fdroid.asc /usr/share/keyrings/fdroid.asc
ADD src/fdroid.list /etc/apt/sources.list.d/fdroid.list

RUN apt-get update \
    && apt-get update \
    && apt-get -y install --no-install-recommends \
      rsync \
      tzdata \
      fdroidserver \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

ADD src/update_repo.sh /

ENTRYPOINT [ "/update_repo.sh" ]
