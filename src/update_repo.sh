#!/bin/bash
mkdir -p /workdir/repo/
rsync -rctv --exclude=".*" /metadata/* /workdir/
rsync -rctv /builds/*.apk /workdir/repo/
fdroid update --use-date-from-apk
fdroid signindex
fdroid deploy
